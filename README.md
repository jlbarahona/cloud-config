# Cloud-Config Collection

This repository contains a collection of cloud-config files (yaml format) used to configure different types of cloud instances.

Further information about each cloud-config file is stored on the project wiki:
https://bitbucket.org/axoninsightdevops/cloud-config/wiki/
